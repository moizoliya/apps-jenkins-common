import java.text.SimpleDateFormat
public class BuildUtility{
    static def createBuildInfo(context, pipelineData, buildConfiguration, environmentName, changeRequestNumber='', deployRequestNumber='') {
           
            def jsonFileContent = ""
            def env = context.env
            def envFullNames = getEnvironmentFullNames()
            jsonFileContent = """{
                           \"applicationName\":\"${getApplicationName(context,pipelineData)}\",
                           \"buildUserId\":\"${getBuildUserId(context)}\",
                           \"buildUser\":\"${getBuildUser(context)}\",
                           \"build\":\"${env.BUILD_NUMBER}\",
                           \"buildConfiguration\":\"${buildConfiguration}\",
                           \"environment\":\"${environmentName}\",
                           \"environmentFullName\":\"${envFullNames[environmentName]}\",
                           \"changeRequestNumber\":\"${changeRequestNumber}\",
                           \"deployRequestNumber\":\"${deployRequestNumber}\",
                           \"git\": {
                                \"url\": \"${context.GIT_URL}\",
                                \"branch\": \"${context.GIT_BRANCH}\",
                                \"commit\": \"${context.GIT_COMMIT}\"
                           },   
                           \"createdOn\":\"${getDateString()}\"
                           }"""
            return jsonFileContent
    }

 
    static def getEnvironmentFullNames(){
        def result = [DEV:'Development', QA:'Staging', PROD: 'Production']
        return result
    }        

    static def getDateString(){
        def dateFormat = new SimpleDateFormat("yyyyMMddHHmm")
        def date = new Date()
        return dateFormat.format(date)
    }


    static def createArtifactName(context, environmentName){
        def env = context.env
        return "${environmentName.toUpperCase()}-${env.JOB_NAME}-${env.BUILD_NUMBER}.zip"
    }

    static def getApplicationName(context, pipelineData){
        def env = context.env
        def result = pipelineData.APPLICATION_NAME
        if (result==null || result=="") {
             result = env.JOB_NAME
        }
        return result
    }

    
    static def getBuildUserId(context){
        def result = ""
        context.wrap([$class: 'BuildUser']) {
            result = context.env.BUILD_USER_ID
        }
        return result
    }

    static def getBuildUser(context){
        def result = ""
        context.wrap([$class: 'BuildUser']) {
            result = context.env.BUILD_USER
        }
        return result
    }

    static def getSourceContentFolder(pipelineData){
        def result = pipelineData.SRC_DIR
        if(result==null || result=="")
        {
            return "."
        }
        else{
            return result
        }
    }

    static def getBuildConfigName(pipelineData, buildConfiguration, environmentName ){
        def useBuildConfig = buildConfiguration
        if(useBuildConfig =='Environment'){
            useBuildConfig = environmentName
        }
        if(pipelineData!=null && pipelineData.BUILD_CONFIGURATION!=null &&  pipelineData.BUILD_CONFIGURATION[environmentName]!=null && pipelineData.BUILD_CONFIGURATION[environmentName].NAME!=null )  {
                useBuildConfig = pipelineData.BUILD_CONFIGURATION[environmentName].NAME
        }
        return useBuildConfig
    }

    
    static def getBuildFolderDrop(pipelineData, environmentName ){
        def result = null
        if(pipelineData!=null && pipelineData.BUILD_CONFIGURATION!=null &&  pipelineData.BUILD_CONFIGURATION[environmentName]!=null && pipelineData.BUILD_CONFIGURATION[environmentName].FOLDER_DROP!=null )  {
                result = pipelineData.BUILD_CONFIGURATION[environmentName].FOLDER_DROP
        }
        return result
    }
}