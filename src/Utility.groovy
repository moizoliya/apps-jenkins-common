import java.text.SimpleDateFormat
public class Utility{
    
    static def createObject(body) {
        def result = [:]
        body.resolveStrategy = Closure.DELEGATE_FIRST
        body.delegate = result
        body()
        return result
    }
        
    
    static def trimFolderPath(path) {
        def pathLastChar = path[-1..-1]
        def result = pathLastChar=="/" || pathLastChar == "\\" ?  path.substring(0, path.length() - 1 ) : path;
        return result
    }
        
    static def pathCombine(path1, path2){
        if(path1.endsWith("/")){
            return "${path1}${path2}";
        }
        else {
            return "${path1}\\${path2}";
        }
        
    }   

    
    static def getTimeStampString(){
        def dateFormat = new SimpleDateFormat("yyyyMMddHHmmssSSS")
        def date = new Date()
        return dateFormat.format(date)
    }
}