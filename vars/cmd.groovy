def call(script){
    if (isUnix()){
        sh script:script
    }
    else {
        bat script:script
    }

}