
// Build Pipeline - for dotnet Windows application installer
def call(body){
        // evaluate the body block, and collect configuration into the object
        def pipelineData = [:]
        body.resolveStrategy = Closure.DELEGATE_FIRST
        body.delegate = pipelineData
        body()
        pipelineData.SOLUTION_PATH = ".\\${pipelineData.SOLUTION_PATH}" // Solution Path
        pipeline {
            agent {
                node { label '' }
            }
            parameters {
                    //string(name: 'BRANCH', defaultValue:"develop", description: "Which branch would you like to build ?")
                    gitParameter branchFilter: 'origin/(.*)', defaultValue: 'develop', name: 'BRANCH', type: 'PT_BRANCH'
                    string(name: 'CHANGE_REQUEST_NUMBER',  defaultValue:'', description: "What is the ITSM (ServiceNow, JIRA etc) change request number ?")
                    choice(name: 'BUILD_CONFIGURATION',   choices: ['Environment'], description: 'What is the Build Configuration ?')
            }
            
            
            stages {
                
                stage('Build') {
                    steps {
                        echo 'Running Pipeline as : '                        
                        echo "Build Triggered by : ${BuildUtility.getBuildUserId(this)}"
                        echo "Building Source Code using Branch : ${params.BRANCH}"  
                        echo "Building Source Code using Build Configuration : ${params.BUILD_CONFIGURATION}"  
                        dir(BuildUtility.getSourceContentFolder(pipelineData)){
                            build_app(pipelineData)    
                        }

                    }
                }
                stage('Test') {
                    steps {
                        echo "Testing  .."
                        dir(BuildUtility.getSourceContentFolder(pipelineData)){
                            run_test(pipelineData)   
                        }

                    }
                }
                stage('Deploy-DEV') {
                    steps {
                        deploy pipelineData, params, "DEV", false
                    }
                }
                stage('Deploy-QA') {
                    steps {
                        deploy pipelineData, params, "QA", false
                    }
                }
                stage('Deploy-PROD') {
                    steps {
                        deploy pipelineData, params, "PROD", true
                    }
                }

            }

            post {
                aborted {
                    echo 'Deployment Timed out'
                }
                    

                always {
                  cleanUp()
                }                             
            }


        }

}
 

def get_script_command_line(pipelineData, buildConfiguration, environmentName)
{
    def result = "${params.BRANCH} ${buildConfiguration} ${environmentName} ${env.BUILD_NUMBER}"
    return result;
}

def cleanUp(){
  deleteDir()
 }

def build_app(pipelineData) {
    echo "Building at node - ${env.NODE_NAME} .."
    // cmd "dotnet restore"
    // cmd "dotnet build"
    clean_build(pipelineData)
    cmd "nuget restore \"${pipelineData.SOLUTION_PATH}\""
    cmd createMSBuildCommand(pipelineData,"Debug")
}

def clean_build(pipelineData){
    cmd "msbuild \"${pipelineData.SOLUTION_PATH}\" /t:clean ${getWixCommandLineArg()}"
}

def run_test(pipelineData){
    echo "Testing  ${pipelineData.TEST_PROJECT}.."
    def testProject = pipelineData.TEST_PROJECT
    if(testProject==null || testProject==""){
        echo "Skipping Test as no Test Project Information was passed"
    }
    else{
        cmd "dotnet test  \"${testProject}\"   --logger \"trx;LogFileName=unit_tests.xml\" --no-build"
        step([$class: 'MSTestPublisher', testResultsFile:"**/unit_tests.xml", failOnError: true, keepLongStdio: true])

    }

}

def write_build_info(pipelineData, buildConfiguration, environmentName, changeRequestNumber='', deployRequestNumber=''){
    echo "Writing Build Information for ${environmentName} Build"                           
    def buildInfoText = BuildUtility.createBuildInfo(this, pipelineData, buildConfiguration, environmentName, changeRequestNumber, deployRequestNumber)
    writeFile file: "./${buildOutputFolder}/build.json",  text: buildInfoText // Write the Information in AssemblyInfo files of all the projects
}

 
// ---------------------------------------------------------------------------------------------------------------------------------------------------
// BUILD DEPLOY FUNCTIONS
// ---------------------------------------------------------------------------------------------------------------------------------------------------
def deploy(pipelineData, params, environmentName, validate){
                            
        dir(BuildUtility.getSourceContentFolder(pipelineData)){                            

            if(!validate){
                            build_and_deploy_app_for_environment (pipelineData, params.BUILD_CONFIGURATION, environmentName, params.CHANGE_REQUEST_NUMBER)    
            }
            else{
                            
                            def changeRequestNumber = params.CHANGE_REQUEST_NUMBER;
                            def deployRequestNumber = ''
                            timeout(time: 600, unit: 'SECONDS') { 
                                deployRequestNumber = input message: "${environmentName} Deployment Details ?", ok: 'Validate Request', parameters: [password(name: 'PROD_DEPLOY_REQUEST_NUMBER', description: 'What is the ITSM (ServiceNow, JIRA etc) Deployment Request Number ?')]
                            }
                            echo "Service Now Request Number from User : ${deployRequestNumber}"
                            
                            
                            if(ValidationUtility.IsDeploymentRequestValid(env, environmentName, changeRequestNumber, deployRequestNumber))  {
                                build_and_deploy_app_for_environment (pipelineData, params.BUILD_CONFIGURATION, environmentName, changeRequestNumber, deployRequestNumber)   
                            }     
                            else{
                                error "Invalid Service Now Request Number for Deployment to ${environmentName}"
                            }
            }                            
    }
}
def build_and_deploy_app_for_environment(pipelineData, buildConfiguration, environmentName, changeRequestNumber='', deployRequestNumber=''){
    def useBuildConfig = BuildUtility.getBuildConfigName(pipelineData, buildConfiguration, environmentName)
    def cmdLineArg = get_script_command_line (pipelineData, buildConfiguration, environmentName); // set script vairbales
    def archiveArtifactName = BuildUtility.createArtifactName(this, environmentName) // create Archive Artifact Name


    echo "Building Source Code for environment : ${environmentName}"  
    echo "Build Configuration : ${buildConfiguration}"
    // Change Assembly Info & Write Build Info
    def srcBranch = this.GIT_BRANCH
    def srcCommit = this.GIT_COMMIT
    def buildBy = BuildUtility.getBuildUser(this)
    def assemDesc = "BuildOn: ${BuildUtility.getDateString()}, BuildBy: ${buildBy}, BuildConfiguration: ${useBuildConfig}, Environment: ${environmentName}, SourceBranch: ${srcBranch}, SourceCommit: ${srcCommit}"
    echo "Writing Assembly Description : ${assemDesc}"
    changeAsmVer assemblyCompany: '', assemblyCopyright: '', assemblyCulture: '', assemblyDescription: assemDesc, assemblyFile: '', assemblyProduct: '', assemblyTitle: '', assemblyTrademark: '', regexPattern: 'Assembly(\\w*)Version\\("([0-9]+)\\.([0-9]+)\\.([0-9]+)\\.([0-9]+)"\\)', replacementPattern: 'Assembly$1Version("$2.$3.$4.%s")', versionPattern: '${BUILD_NUMBER}'
    clean_build(pipelineData) // clean
    run_script_config (pipelineData, 'PRE_BUILD', cmdLineArg)    // PRE BUILD SCRIPT
    cmd createMSBuildCommand(pipelineData,useBuildConfig)
    run_script_config (pipelineData, 'POST_BUILD', cmdLineArg) // POST BUILD SCRIPT

    // Create Deployable Folder
    // Archive
    def useSourceFolder = createDeployFolder(environmentName,pipelineData) // Create Installation Artefactfolder
    def installerFilePattern = "**/${useBuildConfig}/*.msi"
    fileOperations([
        fileCopyOperation (flattenFiles: true, includes: installerFilePattern, targetLocation : useSourceFolder)
    ])
    archive_content (useSourceFolder, archiveArtifactName,  "") // Zip the Published Build OUtput

    // Drop to location 
    def folderDropPath = BuildUtility.getBuildFolderDrop(pipelineData, environmentName )
    folderDrop(useSourceFolder, folderDropPath) // Drop the build to location

    dir(useSourceFolder){
          deleteDir()
    }

}

   def folderDrop(sourceFolderPath, destinationFolderPath){
       if(destinationFolderPath==null || destinationFolderPath=='')
       {
           return;
       }
    fileOperations([
        folderCopyOperation( destinationFolderPath: "${destinationFolderPath}", sourceFolderPath: sourceFolderPath)
    ])
   } 
 

  def createDeployFolder(deployConfigKey, options){
    def appName = BuildUtility.getApplicationName(this, options)
    def deployTimeStamp = Utility.getTimeStampString()
    def deployEnvFolderName = "${appName}_${deployConfigKey}_${deployTimeStamp}_deploy_artefacts"
    def tempFile = File.createTempFile("jenkins", deployEnvFolderName);
    def parentDirPath = tempFile.getParent()
    tempFile.delete() // Delete TempFile Not Needed as it was used only to get the Temp Directory Path
    def useSourceFolder = Utility.pathCombine(parentDirPath, deployEnvFolderName) // Create Temp Directory Location
    return useSourceFolder
 }

 



 
def createMSBuildCommand(pipelineData, buildConfig){
    
    //def platformArg = buildConfig.toUpperCase()!="DEBUG"? "/p:Platform=\"${params.BUILD_PLATFORM}\"" : ""
    
    return "msbuild \"${pipelineData.SOLUTION_PATH}\" /p:Configuration=${buildConfig} ${getWixCommandLineArg()}" // Build the output
}

def getWixCommandLineArg(){
    def wixBasePath = "F:\\wix311-binaries" // replace with value from Global Key Store
    def msBuildWixArg="/p:WixCATargetsPath=\"${wixBasePath}\\sdk\\wix.ca.targets\" /p:WixTargetsPath=\"${wixBasePath}\\wix.targets\"";
    return msBuildWixArg
}
