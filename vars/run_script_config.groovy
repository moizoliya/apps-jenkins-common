def call(pipelineData, scriptName, cmdLineArg ){
    if(pipelineData!=null && pipelineData.SCRIPT_CONFIG!=null && pipelineData.SCRIPT_CONFIG[scriptName]!=null)  {
        def scripFileName =  pipelineData.SCRIPT_CONFIG[scriptName]
        cmd  scripFileName + " ${cmdLineArg}"
    }
}
