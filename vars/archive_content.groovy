def call(contentSource,fileName, globPattern){
   zip archive: true, dir: contentSource, glob: globPattern, zipFile: fileName
   
   delete_file fileName
}