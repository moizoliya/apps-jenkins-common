def call(newFile, replaceFile){
    if (isUnix()){
        sh script:"mv -f ${newFile} ${replaceFile}"
    }
    else {
        def src = newFile.replace("/","\\")
        def target = replaceFile.replace("/","\\")
        bat script:"move /Y \"${src}\" \"${target}\""
    }

}