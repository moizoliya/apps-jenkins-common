
def call(environmentName, sourceFolder, options){
    def deploy_Host = "" // Server
    def deploy_Destination = "" // "SiteName/Application_Name" -- Site Name where it has to be deployed
    def deploy_Cred_Id = ""
    def envName = environmentName.toUpperCase() 
    def deploy_Settings = null
    def deployEnvSrcFolderList = [:] 



    // Build Deploy Config from Closure
    def deployConfig = options.DEPLOY_CONFIG
        if (deployConfig != null){
            echo "Using Deployment Configuration "            
            // Create All Environment specific copies of the same Artificat
            deployConfig.each{ key, value  -> 
                if(key.startsWith(envName)){
                    def envInstanceDeployFolder = createDeployFolder(key, sourceFolder, options)                 
                    deployEnvSrcFolderList[key] = envInstanceDeployFolder // Folder for Every Environment Instance
                }
            }


            // Clean all Unwanted file - IF deployment is for PROD - it will clear all DEV and QA related files
            // Clean all the Unwanted files from all the Folders
            deployConfig.each{ key, value  -> 
                if(!key.startsWith(envName)){
                        deployEnvSrcFolderList.each{ deployEnvKey, deplyEnvSrcFolder  -> 
                            clean_replace_files(key, deplyEnvSrcFolder , deployConfig[key].DEPLOY_SETTINGS)
                        }
                }
            }


            // Deploy for actual configuration
            deployConfig.each{ key, value  -> 
                if(key.startsWith(envName)){ // if the deployConfig object contains anything startswith PROD like PROD_1, PROD_2
                    if (deployConfig[key] != null){
                        deploy_Cred_Id = deployConfig[key].DEPLOY_CRED
                        deploy_Host = deployConfig[key].DEPLOY_HOST 
                        deploy_Destination =  deployConfig[key].DEPLOY_DESTINATION
                        deploy_Settings = deployConfig[key].DEPLOY_SETTINGS
                        echo "Starting Deployment for : ${key}"    
                        def useSourceFolder = deployEnvSrcFolderList[key]
                        deploy (key, envName, useSourceFolder, deploy_Host, deploy_Cred_Id, deploy_Destination , deploy_Settings, options);
                    }
                }
            }
           
        }
        else{ // default deployment 
            echo "Starting Deployment for : ${envName}"    
            deploy (envName, envName, sourceFolder, deploy_Host, deploy_Cred_Id, deploy_Destination, deploy_Settings, options);
        }

        
}
def clean_replace_files(deployConfigKey,sourceFolder, deploy_Settings) {
     if(deploy_Settings!=null){
                // Replace Files
                if(deploy_Settings.REPLACE_FILES!=null){
                    echo "Cleaning Files For Non Target Deployment for : ${deployConfigKey}"    
                    dir(sourceFolder){
                        deploy_Settings.REPLACE_FILES.each{ key, value  -> 
                                delete_file (value)
                        }
                    }
                }

    
        }
}
def replace_files(deployConfigKey, envName, sourceFolder, deploy_Host, deploy_Cred_Id, deploy_Destination, deploy_Settings, options)
{
       if(deploy_Settings!=null){
           // Replace Files
                if(deploy_Settings.REPLACE_FILES!=null){
                    echo "Replacing Files For Deployment for : ${deployConfigKey}"    
                    dir(sourceFolder){
                        deploy_Settings.REPLACE_FILES.each{ key, value  -> 
                            move_file (value, key)
                        }
                    }
                    
                }

                // Delete files
                if(deploy_Settings.DELETE_FILES!=null){
                    echo "Deleting Files For Target Deployment for : ${deployConfigKey}"    
                    dir(sourceFolder){
                        deploy_Settings.DELETE_FILES.each{  
                            if(fileExists(it)){
                                echo "Deleting file : $it"
                                delete_file (it)
                            }
                         }
                    }
                }
        }
                        
}

def modify_build_config(deployConfigKey, envName, sourceFolder, deploy_Host, deploy_Cred_Id, deploy_Destination, deploy_Settings, options){
    def buildFileName = 'build.json'
    
    dir(sourceFolder){
        echo "Modifying Build Config at Directory : ${sourceFolder}"
        echo "Modifying Build Config at Deploy Config Key : ${deployConfigKey}"
        cmd "dir"
        def deployInfo = [environment: deployConfigKey, host: deploy_Host, destination: deploy_Destination.toString() ]
        def props = readJSON file: buildFileName
        props["deploy"] = deployInfo
        writeJSON file: buildFileName, json: props
    }
}

def deploy(deployConfigKey, envName, sourceFolder, deploy_Host, deploy_Cred_Id, deploy_Destination, deploy_Settings, options){
    def deployConfig = options.DEPLOY_CONFIG
    def archiveArtifactName = BuildUtility.createArtifactName(this, deployConfigKey) // create Archive Artifact Name
    def useSourceFolder = sourceFolder
    echo "Deploying from : ${useSourceFolder}"
    
    // REPLACE Deployment Specific files
    replace_files (deployConfigKey, envName, useSourceFolder, deploy_Host, deploy_Cred_Id, deploy_Destination , deploy_Settings, options);

      // Credential ID
    if(deploy_Cred_Id==null || deploy_Cred_Id=="")
    {
        deploy_Cred_Id = "${envName}_DEPLOY_CRED"
    }
 

    // Hosting
    if(deployConfig!=null && (deploy_Host=="" || deploy_Host == null)){
        deploy_Host = deployConfig["DEFAULT_DEPLOY_HOST_IIS"]
    }
    
    if(deploy_Host=="" || deploy_Host == null){
        deploy_Host = env.DEFAULT_DEPLOY_HOST_IIS
    }
    
    // Website name
    if(deploy_Destination =="" || deploy_Destination == null){
        deploy_Destination = "${envName}/${BuildUtility.getApplicationName(this, options)}"
    }

    // Modify Config
    modify_build_config (deployConfigKey, envName, useSourceFolder, deploy_Host, deploy_Cred_Id, deploy_Destination, deploy_Settings, options)

    // Deploy to server
    dir(useSourceFolder){
        def cmdLineArg = get_script_command_line (options, params.BUILD_CONFIGURATION, envName); // set script vairbales
        run_script_config (options, 'PRE_DEPLOY', cmdLineArg) // POST BUILD SCRIPT
        delete_configured_script (options) // delete all files
        // create logs directory that will be part of publish package
        dir("logs"){
            writeFile file: "./.log",  text: ""
        }   
         withCredentials([usernamePassword(
            credentialsId: "${deploy_Cred_Id}",
            usernameVariable: "USER",
            passwordVariable: "PASS"
            )]) {
                def skipDeleteRule1 = "-skip:skipAction=Delete,objectName=filePath,absolutePath=.\\\\*\\.log"
                def skipDeleteRule2 = "-skip:skipAction=Delete,objectName=dirPath,absolutePath=App_Data"
                def skipDeleteRule3 = "-skip:objectName=filePath,absolutePath=.\\\\*\\.cmd"
                def preRunCommand = getRunCommand(deploy_Settings, "PRE_DEPLOY")
                def postRunCommand = getRunCommand(deploy_Settings, "POST_DEPLOY")

                if(preRunCommand!=null){
                    if(preRunCommand instanceof GString){
                        cmd "msdeploy -verb:sync ${preRunCommand} -dest:auto,computername=${deploy_Host},username=${USER},password=${PASS}"
                    }
                    else{
                        preRunCommand.each {
                            def runCommand = formatRunCommand(it)
                            cmd "msdeploy -verb:sync ${runCommand} -dest:auto,computername=${deploy_Host},username=${USER},password=${PASS}"    
                        }
                    }
                    
                }
                
                // Deploy Artifacts
                cmd "msdeploy -verb:sync -source:iisApp=\"${pwd()}\"  ${skipDeleteRule1} ${skipDeleteRule2} ${skipDeleteRule3} -dest:iisApp=\"${deploy_Destination}\",wmsvc=${deploy_Host},username=${USER},password=${PASS},skipAppCreation=false -allowUntrusted=true -enableRule:AppOffline"

                if(postRunCommand!=null){
                    if(postRunCommand instanceof GString){
                        cmd "msdeploy -verb:sync ${postRunCommand} -dest:auto,computername=${deploy_Host},username=${USER},password=${PASS}"    
                    }
                    else{
                        postRunCommand.each {
                            def runCommand = formatRunCommand(it)
                            //cmd "msdeploy -verb:sync ${runCommand} -dest:auto,computername=${deploy_Host},username=${USER},password=${PASS}"    
                            cmd "msdeploy -verb:sync ${runCommand} -dest:iisApp=\"${deploy_Destination}\",wmsvc=${deploy_Host},username=${USER},password=${PASS}"    
                        }
                    }
                    
                }
                
            }
    }

    // Archive
    archive_content (useSourceFolder, "${archiveArtifactName}", "") // Zip the Published Build OUtput

    // Delete Environment specific Deployment Directory created
    dir(useSourceFolder){
          deleteDir()
    }
}

 
 def getRunCommand(deploy_Settings, commandKey){
     def result = null 
     if(deploy_Settings!=null && deploy_Settings.RUN_COMMANDS!=null && deploy_Settings.RUN_COMMANDS[commandKey]!=null)  {
        result =  deploy_Settings.RUN_COMMANDS[commandKey]
        if(result!=null &&  result instanceof String){
            return formatRunCommand(result)
        }
    }
    return result
 }
 def formatRunCommand(cmd){
    def result = cmd.trim()
    def isCommandOrBatchRun = result.endsWith(".bat")  || result.endsWith(".cmd") 
    if(result.startsWith("./") && isCommandOrBatchRun){
        result = Utility.pathCombine(pwd(), result.substring(2)) // Add the patj
    }
    return "-source:runcommand=\"${result}\" -allowUntrusted:true"   
 }   

 

 def createDeployFolder(deployConfigKey, sourceFolder, options){
    def appName = BuildUtility.getApplicationName(this, options)
    def deployTimeStamp = Utility.getTimeStampString()
    def deployEnvFolderName = "${appName}_${deployConfigKey}_${deployTimeStamp}_deploy_artefacts"
    def tempFile = File.createTempFile("jenkins", deployEnvFolderName);
    def parentDirPath = tempFile.getParent()
    tempFile.delete() // Delete TempFile Not Needed as it was used only to get the Temp Directory Path
    def useSourceFolder = Utility.pathCombine(parentDirPath, deployEnvFolderName) // Create Temp Directory Location
    echo "Copying files to Deployment Directory  : ${deployEnvFolderName}"
    echo "Copying Source Directory Directory  : ${sourceFolder}"
    dir(sourceFolder){
      fileOperations([
                    // fileDownloadOperation(
                    //         password: '',
                    //         targetFileName: 'foo.zip',
                    //         targetLocation: "${WORKSPACE}",
                    //         url: 'https://foobar.com/foo.zip',
                    //         userName: ''),
                    // fileUnZipOperation(
                    //         filePath: 'foo.zip',
                    //         targetLocation: '.'),
                    folderCopyOperation( destinationFolderPath: "${useSourceFolder}", sourceFolderPath: ".")
              ])

    }
    return useSourceFolder
 }


 def get_script_command_line(pipelineData, buildConfiguration, environmentName)
{
    def result = "${params.BRANCH} ${buildConfiguration} ${environmentName} ${env.BUILD_NUMBER}"
    return result;
}
