
// Build Pipeline - for deploying Artefact directly
def call(body){
        // evaluate the body block, and collect configuration into the object
        def pipelineData = [:]
        body.resolveStrategy = Closure.DELEGATE_FIRST
        body.delegate = pipelineData
        body()
        
        pipeline {
            agent {
                node { label '' }
            }
            parameters {
                    //string(name: 'BRANCH', defaultValue:"develop", description: "Which branch would you like to build ?")
                    gitParameter branchFilter: 'origin/(.*)', defaultValue: 'develop', name: 'BRANCH', type: 'PT_BRANCH'
                    string(name: 'CHANGE_REQUEST_NUMBER',  defaultValue:'', description: "What is the ITSM (ServiceNow, JIRA etc) change request number ?")
            }
            
            
            stages {
                
                stage('Build') {
                    steps {
                        echo 'Running Pipeline as : '                        
                        echo "Triggered by : ${BuildUtility.getBuildUserId(this)}"
                        echo "Artefact using Branch : ${params.BRANCH}"  
                    }
                }
                
                stage('Archieve') {
                    steps {
                        echo 'Archieving..'
                        dir(BuildUtility.getSourceContentFolder(pipelineData)){
                            archive_content (".", "${BuildUtility.getApplicationName(this, pipelineData)}-${env.BUILD_NUMBER}.zip","")
                        }
                    }
                }
                stage('Deploy-DEV') {
                    steps {
                        deploy pipelineData, params, "DEV", false
                    }
                }
                stage('Deploy-QA') {
                    steps {
                        deploy pipelineData, params, "QA", false
                    }
                }
                stage('Deploy-PROD') {
                    
                    when {
                        beforeAgent true
                        expression {params.BRANCH == 'master'}
                    }
                    steps {
                        deploy pipelineData, params, "PROD", true
                    }
                }

            }

            post {
                aborted {
                    echo 'Deployment Timed out'
                }
                    

                always {
                  cleanUp()
                }                             
            }


        }

}
 



def get_script_command_line(pipelineData, buildConfiguration, environmentName)
{
    def result = "${params.BRANCH} ${buildConfiguration} ${environmentName} ${env.BUILD_NUMBER}"
    return result;
}

def cleanUp(){
  deleteDir()
}



def write_build_info(pipelineData, buildConfiguration, environmentName, changeRequestNumber='', deployRequestNumber=''){
    echo "Writing Build Information for ${environmentName} Build"                           
    def buildInfoText = BuildUtility.createBuildInfo(this, pipelineData, buildConfiguration, environmentName, changeRequestNumber, deployRequestNumber)
    writeFile file: "./build.json",  text: buildInfoText
}

 
// ---------------------------------------------------------------------------------------------------------------------------------------------------
// BUILD DEPLOY FUNCTIONS
// ---------------------------------------------------------------------------------------------------------------------------------------------------
def deploy(pipelineData, params, environmentName, validate){
                            
        dir(BuildUtility.getSourceContentFolder(pipelineData)){                            

            if(!validate){
                            build_and_deploy_app_for_environment (pipelineData, params.BUILD_CONFIGURATION, environmentName, params.CHANGE_REQUEST_NUMBER)    
            }
            else{
                            
                            def changeRequestNumber = params.CHANGE_REQUEST_NUMBER;
                            def deployRequestNumber = ''
                            timeout(time: 600, unit: 'SECONDS') { 
                                deployRequestNumber = input message: "${environmentName} Deployment Details ?", ok: 'Validate Request', parameters: [password(name: 'PROD_DEPLOY_REQUEST_NUMBER', description: 'What is the ITSM (ServiceNow, JIRA etc) Deployment Request Number ?')]
                            }
                            echo "Service Now Request Number from User : ${deployRequestNumber}"
                            
                            
                            if(ValidationUtility.IsDeploymentRequestValid(env, environmentName, changeRequestNumber, deployRequestNumber))  {
                                build_and_deploy_app_for_environment (pipelineData, params.BUILD_CONFIGURATION, environmentName, changeRequestNumber, deployRequestNumber)   
                            }     
                            else{
                                error "Invalid Service Now Request Number for Deployment to ${environmentName}"
                            }
            }                            
    }
}
def build_and_deploy_app_for_environment(pipelineData, buildConfiguration, environmentName, changeRequestNumber='', deployRequestNumber=''){
    
    def useBuildConfig = BuildUtility.getBuildConfigName(pipelineData, buildConfiguration, environmentName)
    def buildOutputFolder = "${pwd()}"
    def cmdLineArg = get_script_command_line (pipelineData, buildConfiguration, environmentName); // set script vairbales
    echo "Building Artefact for environment : ${environmentName}"  
    echo "Build Configuration : ${buildConfiguration}"
    run_script_config (pipelineData, 'PRE_BUILD', cmdLineArg)    // PRE BUILD SCRIPT
    write_build_info (pipelineData, buildConfiguration, environmentName, changeRequestNumber, deployRequestNumber)
    run_script_config (pipelineData, 'POST_BUILD', cmdLineArg) // POST BUILD SCRIPT
    deploy_app_iis (environmentName, buildOutputFolder, pipelineData)   // Deploy to Server
    
 
}


 
 