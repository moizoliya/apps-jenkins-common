def call(file){
    
    if (isUnix()){
        sh script:"rm \"${file}\""
    }
    else {
        def target = file.replace("/","\\")
        bat script:"del \"${target}\""
    }

}