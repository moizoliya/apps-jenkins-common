
// Build Pipeline - for .NET Core 2.0 Web application
def call(body){
        // evaluate the body block, and collect configuration into the object
        def pipelineData = [:]
        body.resolveStrategy = Closure.DELEGATE_FIRST
        body.delegate = pipelineData
        body()
        
        pipeline {
            agent {
                node { label '' }
            }
            parameters {
                    //string(name: 'BRANCH', defaultValue:"develop", description: "Which branch would you like to build ?")
                    gitParameter branchFilter: 'origin/(.*)', defaultValue: 'develop', name: 'BRANCH', type: 'PT_BRANCH'
                    string(name: 'CHANGE_REQUEST_NUMBER',  defaultValue:'', description: "What is the ITSM (ServiceNow, JIRA etc) change request number ?")
                    choice(name: 'BUILD_CONFIGURATION',   choices: ['Release', 'Debug', 'Environment'], description: 'What is the Build Configuration ?')
            }
            
            
            stages {
                
                stage('Build') {
                    steps {
                        echo 'Running Pipeline as : '                        
                        echo "Build Triggered by : ${BuildUtility.getBuildUserId(this)}"
                        echo "Building Source Code using Branch : ${params.BRANCH}"  
                        echo "Building Source Code using Build Configuration : ${params.BUILD_CONFIGURATION}"  
                        dir(BuildUtility.getSourceContentFolder(pipelineData)){
                            build_app(pipelineData)    
                        }

                    }
                }
                stage('Test') {
                    steps {
                        echo "Testing  .."
                        dir(BuildUtility.getSourceContentFolder(pipelineData)){
                            run_test(pipelineData)   
                        }

                    }
                }
                
                stage('Archieve') {
                    steps {
                        echo 'Archieving..'
                        dir(BuildUtility.getSourceContentFolder(pipelineData)){
                            archive_content (".", "${BuildUtility.getApplicationName(this, pipelineData)}-${env.BUILD_NUMBER}.zip", "")
                        }
                    }
                }
                stage('Deploy-DEV') {
                    steps {
                        deploy pipelineData, params, "DEV", false
                    }
                }
                stage('Deploy-QA') {
                    steps {
                        deploy pipelineData, params, "QA", false
                    }
                }
                stage('Deploy-PROD') {
                    
                    when {
                        beforeAgent true
                        expression {params.BRANCH == 'master'}
                    }
                    steps {
                        deploy pipelineData, params, "PROD", true
                    }
                }

            }

            post {
                aborted {
                    echo 'Deployment Timed out'
                }
                    

                always {
                  cleanUp()
                }                             
            }


        }

}
 

def get_script_command_line(pipelineData, buildConfiguration, environmentName)
{
    def result = "${params.BRANCH} ${buildConfiguration} ${environmentName} ${env.BUILD_NUMBER}"
    return result;
}

def cleanUp(){
  deleteDir()
 }

def build_app(pipelineData) {
    echo "Building at node - ${env.NODE_NAME} .."
    // cmd "dotnet restore"
    // cmd "dotnet build"
    cmd "msbuild /t:restore"
    cmd "msbuild /t:build"
}

def run_test(pipelineData){
    echo "Testing  ${pipelineData.TEST_PROJECT}.."
    def testProject = pipelineData.TEST_PROJECT
    if(testProject==null || testProject==""){
        echo "Skipping Test as no Test Project Information was passed"
    }
    else{
        cmd "dotnet test  \"${testProject}\"   --logger \"trx;LogFileName=unit_tests.xml\" --no-build"
        step([$class: 'MSTestPublisher', testResultsFile:"**/unit_tests.xml", failOnError: true, keepLongStdio: true])

    }

}

def write_build_info(pipelineData, buildConfiguration, environmentName, changeRequestNumber='', deployRequestNumber=''){
    echo "Writing Build Information for ${environmentName} Build"                           
    def buildInfoText = BuildUtility.createBuildInfo(this, pipelineData, buildConfiguration, environmentName, changeRequestNumber, deployRequestNumber)

    def buildOutputFolder = get_build_output_folder(pipelineData)
    //writeFile file: "./${get_webproject_name(pipelineData)}/build.json",  text: buildInfoText
    writeFile file: "./${buildOutputFolder}/build.json",  text: buildInfoText
}

def get_webproject_name(pipelineData){
    return pipelineData.WEB_PROJECT_NAME;
}
// ---------------------------------------------------------------------------------------------------------------------------------------------------
// BUILD DEPLOY FUNCTIONS
// ---------------------------------------------------------------------------------------------------------------------------------------------------
def deploy(pipelineData, params, environmentName, validate){
                            
        dir(BuildUtility.getSourceContentFolder(pipelineData)){                            

            if(!validate){
                            build_and_deploy_app_for_environment (pipelineData, params.BUILD_CONFIGURATION, environmentName, params.CHANGE_REQUEST_NUMBER)    
            }
            else{
                            
                            def changeRequestNumber = params.CHANGE_REQUEST_NUMBER;
                            def deployRequestNumber = ''
                            timeout(time: 600, unit: 'SECONDS') { 
                                deployRequestNumber = input message: "${environmentName} Deployment Details ?", ok: 'Validate Request', parameters: [password(name: 'PROD_DEPLOY_REQUEST_NUMBER', description: 'What is the ITSM (ServiceNow, JIRA etc) Deployment Request Number ?')]
                            }
                            echo "Service Now Request Number from User : ${deployRequestNumber}"
                            
                            
                            if(ValidationUtility.IsDeploymentRequestValid(env, environmentName, changeRequestNumber, deployRequestNumber))  {
                                build_and_deploy_app_for_environment (pipelineData, params.BUILD_CONFIGURATION, environmentName, changeRequestNumber, deployRequestNumber)   
                            }     
                            else{
                                error "Invalid Service Now Request Number for Deployment to ${environmentName}"
                            }
            }                            
    }
}
def build_and_deploy_app_for_environment(pipelineData, buildConfiguration, environmentName, changeRequestNumber='', deployRequestNumber=''){
    def publishProfile = get_publish_profile(pipelineData)
    def useBuildConfig = BuildUtility.getBuildConfigName(pipelineData, buildConfiguration, environmentName)
    def buildOutputFolder = get_build_output_folder(pipelineData)
    def cmdLineArg = get_script_command_line (pipelineData, buildConfiguration, environmentName); // set script vairbales
    echo "Building Source Code for environment : ${environmentName}"  
    echo "Build Configuration : ${buildConfiguration}"
    cmd "msbuild /t:restore"
    run_script_config (pipelineData, 'PRE_BUILD', cmdLineArg)    // PRE BUILD SCRIPT
    cmd "msbuild /p:DeployOnBuild=true  /p:PublishProfile=\"${publishProfile}\"  /p:Configuration=${useBuildConfig}" // Build the output
    write_build_info (pipelineData, buildConfiguration, environmentName, changeRequestNumber, deployRequestNumber) // write build info json
    run_script_config (pipelineData, 'POST_BUILD', cmdLineArg) // POST BUILD SCRIPT
    deploy_app_iis (environmentName, buildOutputFolder, pipelineData)   // Deploy to Server
    
}

def get_build_output_folder(pipelineData){
    return "${get_webproject_name(pipelineData)}/${get_publish_folder()}"
}
def get_publish_folder(){
    return "bin/PublishOutput"
}

def get_publish_profile(pipelineData){
    def profileDir = "./${get_webproject_name(pipelineData)}/Properties/PublishProfiles"
    def profileFileName = "folder.pubxml"
    def result =  "${profileDir}/${profileFileName}"
    def fileContent = PublishProfile.dotnetCore(get_publish_folder())
    dir(profileDir){
        writeFile file: profileFileName, text: fileContent  // write a modified pubxml
    }
    
    return result
}

